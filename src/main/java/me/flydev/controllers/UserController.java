package me.flydev.controllers;

import me.flydev.entities.User;
import me.flydev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vuluan on 05/03/015.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/{id}",method = {RequestMethod.GET,RequestMethod.HEAD})
    @ResponseBody
    public List<User> getUser(@PathVariable long id){
        return userRepository.findById(id);
    }

    @RequestMapping(value = "/",method = {RequestMethod.GET,RequestMethod.HEAD})
    @ResponseBody
    public User getUserByUsername(@RequestParam(value = "username") String username){
        return userRepository.findFirstByUsername(username);
    }
}
