package me.flydev.controllers;

import me.flydev.entities.User;
import me.flydev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by vuluan on 06/03/2015.
 */
@Controller
@RequestMapping("/api/user")
public class UserAPIController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/{id}",method = {RequestMethod.GET,RequestMethod.HEAD})
    @ResponseBody
    public List<User> getUser(@PathVariable long id){
        return userRepository.findById(id);
    }
}
