package me.flydev.repositories;

import me.flydev.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vuluan on 05/03/2015.
 */
public interface UserRepository extends JpaRepository<User,Long> {
    List<User> findById(long id);
    User findFirstByUsername(String username);
}
