package me.flydev.services;

import me.flydev.entities.CustomUserDetail;
import me.flydev.entities.User;
import me.flydev.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by vuluan on 06/03/2015.
 */
@Service("customUserDetailService")
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findFirstByUsername(username);
        if(user!=null){
            CustomUserDetail customUserDetail = new CustomUserDetail();
            customUserDetail.setUserName(user.getUsername());
            customUserDetail.setPassword(user.getPassword());
            customUserDetail.setEnabled(true);
            ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));
            customUserDetail.setList(grantedAuthorities);

            return customUserDetail;
        }
        return null;
    }
}
